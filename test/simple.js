// ============================================================================
// Dependencies.
// ============================================================================

// Neiman.
const Neiman = require('..')

// Create the Neiman object.
const neiman = new Neiman(90, 8, 4).style('jazz')
console.log(neiman)
