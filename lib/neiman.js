// ============================================================================
// Dependencies.
// ============================================================================

// Vendor.
const fs = require('fs')
const path = require('path')
const Speaker = require('speaker')
const playAudio = require('audio-play')
const loadAudio = require('audio-loader')

// ============================================================================
// Constants.
// ============================================================================

// Location of the audio files.
const AUDIO_LOCATION = path.resolve(__dirname, '../sounds')

// Extension of the audio files.
const AUDIO_EXTENSION = '.wav'

// Default play options.
const DEFAULT_PLAY_OPTIONS = {
  start: 0,
  loop: true,
  rate: 1,
  detune: 0,
  volume: 1,
  autoplay: true
}

// ============================================================================
// Class definition.
// ============================================================================

class Neiman {
  constructor (bpm = 90, beats = 8, bars = 4, options = DEFAULT_PLAY_OPTIONS) {
    // Assign main class attributes.
    this.setBPM(bpm)
    this.setBeats(beats)
    this.setBars(bars)
    // Set the available sounds and styles by reading sounds folder.
    this.setAvailableStyles()
    this.setAvailableSounds()
    // Init the play options.
    this.setPlayOptions(options)
    // Return the instance.
    return this
  }

  setBPM (bpm) {
    this.bpm = bpm
  }

  setBeats (beats) {
    this.beats = beats
  }

  setBars (bars) {
    this.bars = bars
  }

  setPlayOptions (options) {
    if (this.playOptions) {
      this.playOptions = {
        ...this.playOptions,
        options
      }
    } else this.playOptions = options
  }

  setAvailableStyles () {
    this.styles = fs.readdirSync(AUDIO_LOCATION)
  }

  setAvailableSounds () {
    // Init the sounds class attribute.
    this.sounds = {}
    // Read every style directory.
    this.styles.forEach(style => {
      this.sounds[style] = fs.readdirSync(path.join(AUDIO_LOCATION, style)).map(sound => path.basename(sound, AUDIO_EXTENSION))
    })
  }

  style (style) {
    // If style doesn't exist, throw error.
    if (this.styles.indexOf(style) === -1) throw new Error(`Invalid style ${style}.`)
    // Assign to class attribute.
    this.style = style
    // Init the buffers object.
    this.buffers = {}
    // Populate the buffers object.
    this.sounds[style].forEach(sound => this.buffers[sound] = [])
    // Read all audio files from this style.
    for ( let sound in this.buffers) {
      if (this.buffers.hasOwnProperty(sound)) {
        this.buffers[sound] = fs.readFileSync(path.resolve(AUDIO_LOCATION, this.style, sound + AUDIO_EXTENSION))
      }
    }
    // Return the instance.
    return this
  }

  create (instruments) {
    // TODO.
  }

  play (buffer) {
    // Create the Speaker instance.
    const speaker = new Speaker({
      channels: buffer.numberOfChannels,
      bitDepth: 16,
      sampleRate: buffer.sampleRate
    })
    // PCM data from stdin gets piped into the speaker.
    process.stdin.pipe(speaker)
    // Play sounds.
    return playAudio(buffer, { ...this.playOptions, end: buffer.duration })
  }
}

// Export.
module.exports = Neiman
