# neiman

An audio synthesizer based on real drum sounds. Name inspired by the main character of the Whiplash movie.

## Installation

### Pre-requisites

In Ubuntu systems, you must install `libasound2-dev` in order to compile the dependencies successfully.

```
sudo apt update
sudo apt install libasound2-dev
```

### Installation

Download from the NPM repository.

```
npm i neiman
```